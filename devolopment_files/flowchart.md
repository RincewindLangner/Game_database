# Mermaid file for flow diagram. 

```mermaid
flowchart LR
    start[Start] --> cart[Cartridge]
    start --> dig[Digital]
    start --> disc[Disc]
    start --> oth[Other]

    cart --> plat_cart[Platform cartridge]
    dig --> plat_dig[Platform digital]
    disc --> plat_phy[Platform physical]

    plat_dig --> eshop{eshop}
    eshopMany --> win & lin & mac
    eshop --> win[Windows eshop]
    eshop --> lin[Linux softwear manager]
    eshop --> mac[App store]
    eshop --> eshopMany[More then one eshop]

    plat_dig & plat_cart & plat_phy & win & lin & mac --> return[Return value]
```