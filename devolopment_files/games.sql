BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "disc_online_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "disc_online_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "cartridge_online_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "cartridge_online_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "age_rating" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"board"	TEXT,
	"rating"	TEXT,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "platform_digital_windows" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	TEXT,
	"year"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "platform_digital_mac" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	TEXT,
	"year"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "platform_physical_cartridge" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	TEXT,
	"barcode"	TEXT,
	"year"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "platform_physical_disc" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	TEXT,
	"barcode"	INTEGER,
	"year"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "platform_digital_linux" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	TEXT,
	"year"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "theme" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"theme"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "genre" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"genre"	TEXT,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "digital_windows_local_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "digital_mac_local_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "digital_linux_local_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "digital_linux_local_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "digital_mac_local_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "digital_windows_local_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "cartridge_local_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "cartridge_local_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "disc_local_multiplayer" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "disc_local_coop" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"platform"	INTEGER,
	"number"	INTEGER,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "games" (
	"game_id"	INTEGER,
	"title"	TEXT,
	"decription"	TEXT,
	"publisher"	TEXT,
	"develpoer"	TEXT,
	PRIMARY KEY("game_id")
);
CREATE TABLE IF NOT EXISTS "series" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"series"	TEXT,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
CREATE TABLE IF NOT EXISTS "medium_type" (
	"key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"game_id"	INTEGER,
	"medium"	TEXT,
	FOREIGN KEY("game_id") REFERENCES "games"("game_id")
);
COMMIT;
