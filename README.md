# Game database
A database for cataloging computer games.

NOTE: If you decide to use this program please be aware that it is most likely broken. This is a work (very much) in progress.

## Requirements

* Python 3.10+
    * Currently been devoloped in python 3.11+
* inquirer==2.9.1
    * `pip install inquirer==2.9.1`

## Python virtual environment
If you wish to run this program in a virtual environment please use the script below at the location you want it run it.

### Linux

`python3 -m venv /path/to/new/virtual/environment`

alternative if it doesn't work please see the link below for further instructions.

### Windows
`c:\>c:\Python35\python -m venv c:\path\to\myenv`

### More information

https://docs.python.org/3/library/venv.html

## Description 

Using terminal, the user will be able to enter game data into a game database that will be searchable.

# 

## Road map
1. cli version
* Create new database ^
* Select and open database file ^
* Add entries ^
    * Manual entries ^
* Select entries
* View entries
* Edit entries
* Delete entries
* Search entries

2. GUI version
* Includes above
* Add a GUI version

3. Add webscraper
* Includes above
* Add webscraping from the current user entered data
    * Barcode
    * File name

4. Barcode scanner
* openCV
* physical scanner

^ = completed

$ = part complete
