#!/usr/bin/python3

""" A Game database to catalogue the a
veriaty of games on different platforms."""

# Imports
from source_code.menu import main_menu


def welcome_message():
    """Welcome message."""
    # Welcome code
    print("\nGame database")
    print("\nby Matthew Langner")


# boiler plate code
def main():
    print(__name__)
    if __name__ == "__main__":
        welcome_message()
        main_menu()
    else:
        print("***** it all")


main()
