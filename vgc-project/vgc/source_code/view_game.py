from rich.console import Console
from rich.table import Table

from source_code.search import Db_search
from source_code.db_multi_choice_questions import question_list_selection
from source_code.main_program import make_list_to_dict
from source_code.main_program import tuple_list_to_list
from rich import print as rprint


def view_games(game_file: dict) -> list:
    """_summary_

    :param game_file: _description_
    :type game_file: dict
    :return: _description_
    :rtype: list
    """
    list_all = Db_search(game_file)
    # TODO: Hava a selector for how many games to show.
    offset = 0
    nlimit = -1
    title_games = list_all.db_retive_n_of_game_tiles_acending(offest=offset, nlimit=nlimit)
    title_games = tuple_list_to_list(title_games)
    title_games = make_list_to_dict(title_games)

    explan_view_single_game = question_list_selection(
        title_games,
        "exspand_game_view",
        "Please select which game to expand the view: "
    )

    input("waiting")

    return explan_view_single_game


# def table_single_game(game_name: int):

#     table_1 = Table(expand=True)
#     table_2 = Table(expand=True)
#     table_3 = Table(expand=True)

#     table_1.add_column("Game ID", justify="left", style="")
#     table_1.add_column("Title", justify="left", style="")
#     table_1.add_column("Description", justify="left", style="")
#     table_1.add_column("Publisher", justify="left", style="")
#     table_1.add_column("Developer", justify="left", style="")
#     table_1.add_column("Genre", justify="left", style="")
#     table_1.add_column("Theme", justify="left", style="")
#     table_1.add_column("Game series", justify="left", style="")

#     table_1.add_row("1", f"{game_name}", "one", "", "1")

#     table_2.add_column("Platform", justify="left", style="")
#     table_2.add_column("Medium", justify="left", style="")
#     table_2.add_column("Distribution carrier", justify="left", style="")
#     table_2.add_column("Age rating", justify="left", style="")
#     table_2.add_column("barcode", justify="left", style="")
#     table_2.add_column("single_player", justify="left", style="")

#     table_2.add_row("1", f"{game_name}", "one", "", "1")

#     table_3.add_column("offline_coop_max", justify="left", style="")
#     table_3.add_column("online_coop_max", justify="left", style="")
#     table_3.add_column("offline_multiplayer_max", justify="left", style="")
#     table_3.add_column("online_multiplayer_max", justify="left", style="")

#     table_3.add_row("1", f"{game_name}", "one", "")

#     console = Console()
#     console.print(table_1, table_2, table_3)

#     return


# game_id = 1

# test_one = Db_search("main.sqlite3")
# test_search_one = test_one.db_retrive_one_game(1)
# rprint(test_search_one)


# table_single_game(game_id)
