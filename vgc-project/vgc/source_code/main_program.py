# Holds the bulk of the main program.

from pathlib import Path
from rich import print as rprint

import inquirer

import source_code.db_multi_choice_questions as multi_choice_Q
from source_code.db_creation import create_db_file
from source_code.search import Db_search
from source_code.question_variables import strip_list_for_qa as strip_list


def new_file_name() -> str:
    """Asks the user for a new file name and checks to see if it has already
    been used.
    Returns ether the new file name or False if a files was not created."""
    while True:
        db_file_name = str(input("Please enter your file name. ")).strip()
        if not db_file_name:
            name_reenter = multi_choice_Q.yes_no_option(
                "The file name is blank, re-enter:"
            )
            if name_reenter is False:
                return False
        elif len(db_file_name) >= 1:
            db_file_name += ".sqlite3"
            file_check = check_file_exist(db_file_name)
            if file_check is True:
                file_reenter = multi_choice_Q.yes_no_option(
                    "There is a file currently of that name, re-enter:"
                )
                if file_reenter is False:
                    return False
            elif file_check is False:
                create_db_file(db_file_name)

                return db_file_name
            else:
                print("getting new file name error.")
        else:
            print("Error creating new file.")


def show_directory_files() -> dict:
    """Lists the files in the 'db_save_files' directory."""
    file_list = []
    p = Path("db_save_files")

    for item in p.iterdir():
        # check if it a file
        if item.is_file() and item.match("*.sqlite3"):
            item = item.name
            file_list.append(item)
            file_list.sort()
    return make_list_to_dict(file_list)


def selecting_existing_file(file_list: dict) -> str:
    """Select a file listed in the save directory and return the file name"""
    questions = [
        inquirer.List(
            "files",
            message="Please select the database you want to open",
            choices=lambda x: file_list.values(),
        )
    ]
    answer = inquirer.prompt(questions)

    return answer["files"]


def check_file_exist(file_sqlite: str) -> bool:
    """Check to see if the file exists.
    Include full file name including the extention.
    Return False = no file - True = if there is."""
    return Path("db_save_files", f"{file_sqlite}").exists()


def make_list_to_dict(file_list: list) -> dict:
    """Turns a list into a dictonary with the key starting a int 1."""
    return dict(enumerate(file_list, start=1))


# TODO: fix methods that referance this function.
# Have changed it and it broke other things.
def make_dict_to_list(user_dict: dict) -> list:
    """To convert the dictionary to a list"""
    # TODO: doesn't check if it a list yet in side the values of the dict.
    if len(user_dict.values()) >= 1:
        d = list(user_dict.items())
        # TODO: need to get it so I don't call make dict
        user_dict = make_list_to_dict(d[0][1])
    x = []
    x.extend(user_dict[value] for value in user_dict)
    return x


def list_to_tuple_list(item_list: list) -> list:
    """Takes a list of objects and converts them to
    a tuple list with a comma.

    :param item_list: List to convert to tuple list
    :type item_list: list
    :return: [('Item'), ...]
    :rtype: list
    """
    return [(item,) for item in item_list]


def tuple_list_to_list(item_tuple: list) -> list:
    """Takes a list containing tuples and returns just a list

    :param item_tuple: List that contains sets of tuples
    :type item_tuple: list
    :return: A list
    :rtype: list
    """
    return [item[0] for item in item_tuple]


def barcode(game_data: dict, medium_type: str) -> dict:
    """Get the barcode from the user for each platform selected.
    Takes the game database variable file and which medium as a string."""
    game_platform = game_data.get(medium_type)
    platform_type_name = f"{medium_type}_barcodes"
    sub_platform_item_dict = {}
    for item in game_platform:
        while True:
            get_barcode = input(f"Please input the barcode for the {item}: ")
            data = barcode_check(get_barcode)
            if data == "error":
                user_question = multi_choice_Q.yes_no_option(
                    f"Failed to get valid barcode for the {item}, try again? "
                )
                if user_question is False:
                    break
            elif data:
                sub_platform_item_dict[item] = data
                break
            else:
                break
    platform_dict = {platform_type_name: sub_platform_item_dict}
    game_data |= platform_dict
    return game_data


def barcode_check(barcode: str) -> str:
    """Lets you add a bar code with a lengh of 3 to 20,
    which can include numbers and letters.

    :param barcode: User entered string for barcode
    :type barcode: str
    :return: barcode, string length >=3 and <=20 | "error" if failed
    :rtype: str
    """

    if len(barcode) <= 2 or len(barcode) >= 21:
        return "error"
    elif len(barcode) >= 3 and len(barcode) <= 20:
        return barcode
    else:
        return "error"


# TODO: broken - eshop not converitn to set.
# TODO: finish doc string
def game_eshop_OS(eshop: list) -> set:
    """Checks to see which OS the game is on.

    :param eshop: List of which eshops have been selected
    :type eshop: list
    :return: Intersection result of what has been selected
    :rtype: set
    """
    check_os = {"Linux", "Windows", "MacOS"}
    eshop = set(eshop)
    return eshop.intersection(check_os)


def dictionary_default() -> dict:
    """Sets up the default data struture of the
    dictionary for the data colection.

    :return: Empty dictonary of data entry points
    :rtype: dict
    """
    return {
        "game_id": "",
        "title": "",
        "description": "",
        "publisher": "",
        "platform_other": "",
        "platform_other_user_created": 0,
        "developer": "",
        "medium_of_game": "",
        "game_mode_single": 0,
        "game_mode_coop": 0,
        "game_mode_multiplayer": 0,
        "platform_physical_disc": {},
        "platform_physical_cartridge": {},
        "digital_provider": {},
        "digital_provider_windows": {},
        "digital_provider_linux": {},
        "digital_provider_mac": {},
        "digital_online_multiplayer": {},
        "digital_local_multiplayer": {},
        "digital_online_coop": {},
        "digital_local_coop": {},
        "digital_windows_online_multiplayer": {},
        "digital_windows_local_multiplayer": {},
        "digital_windows_online_coop": {},
        "digital_windows_local_coop": {},
        "digital_linux_online_multiplayer": {},
        "digital_linux_local_multiplayer": {},
        "digital_linux_online_coop": {},
        "digital_linux_local_coop": {},
        "digital_mac_online_multiplayer": {},
        "digital_mac_local_multiplayer": {},
        "digital_mac_online_coop": {},
        "digital_mac_local_coop": {},
        "disc_online_multiplayer": {},
        "disc_local_multiplayer": {},
        "disc_online_coop": {},
        "disc_local_coop": {},
        "cartridge_online_multiplayer": {},
        "cartridge_local_muliplayer": {},
        "cartridge_online_coop": {},
        "cartridge_local_coop": {},
        "platform_other_online_multiplayer": {},
        "platform_other_local_multiplayer": {},
        "platform_other_online_coop": {},
        "platform_other_local_coop": {},
        "theme": "",
        "genres": "",
        "series": "",
        "age_rating": "",
        "age_rating_user_created": 0,
    }


def eshop_os_platform(game_data: dict, whitch_platform_os: set) -> dict:
    """Add the digital game from which OS platform it is on."""
    if "Linux" in whitch_platform_os:
        game_data = eshop_linux(game_data)
    if "Windows" in whitch_platform_os:
        game_data = eshop_win(game_data)
    if "MacOS" in whitch_platform_os:
        game_data = eshop_mac(game_data)

    return game_data


# TODO: need while loops in case of null answer to check if that was correct.
def eshop_linux(game_data: dict) -> dict:
    """Returns the dictiomary of games stores used on Linux"""
    platform_quary_linux = multi_choice_Q.digital_provider_linux()
    game_data |= platform_quary_linux

    return game_data


def eshop_win(game_data: dict) -> dict:
    """Returns the dictiomary of games stores used on Windows"""
    platform_quary_windows = multi_choice_Q.sofware_digital_provider_windows()
    game_data |= platform_quary_windows

    return game_data


def eshop_mac(game_data: dict) -> dict:
    """Returns the dictiomary of games stores used on MacOS"""
    platform_quary_mac = multi_choice_Q.digital_provider_mac()
    game_data |= platform_quary_mac

    return game_data


def age_boards_check(age_board_list: list) -> list:
    # TODO: May not be needed
    """Checks to see which classification board was chosen."""
    boards_list = {
        "PEGI",
        "ESRB",
        "ELSPA",
        "BBFC",
        "CERO",
        "App store",
        "Australian classification board (ACB)",
        "Not rated",
    }
    age_board_list = set(age_board_list)
    return age_board_list.intersection(boards_list)


def age_rating_from_selected_board() -> dict:
    """Checked which age rating board has been selected.
    Returns "key : values" pair."""
    age_rating_boards = multi_choice_Q.get_age_rating_board()
    if age_rating_boards == "PEGI":
        return multi_choice_Q.age_rate_pegi()
    if age_rating_boards == "ESRB":
        return multi_choice_Q.age_rate_esrb()
    if age_rating_boards == "ELSPA":
        return multi_choice_Q.age_rate_elspa()
    if age_rating_boards == "BBFC":
        return multi_choice_Q.age_rate_bbfc()
    if age_rating_boards == "CERO":
        return multi_choice_Q.age_rate_cero()
    if age_rating_boards == "App store":
        return multi_choice_Q.age_rate_app_store()
    if age_rating_boards == "Australian classification board /ACB/":
        return multi_choice_Q.age_rate_aus()
    if age_rating_boards == "Not rated":
        return {"not rated": "not rated"}
    if age_rating_boards == "Other":
        return "other"


def age_rating_other(database_file: str) -> tuple:

    age_rating_user = Db_search(database_file)

    age_user_list = age_rating_user.get_user_created_age_boards()

    if not any(age_user_list):
        return user_enter_age_rating_other()
    elif any(age_user_list):
        age_user_list = strip_list(age_user_list)
        age_user_list.insert(0, "New")
        user_choice = multi_choice_Q.age_rating_user_list(age_user_list)

        if "New" in user_choice.get("age_board_other"):
            return user_enter_age_rating_other()
        elif "New" not in user_choice.get("age_board_other"):
            board = user_choice.get("age_board_other")
            user_created_age_values = age_rating_user.get_user_created_age_values(board)
            user_created_age_values = strip_list(user_created_age_values)
            user_created_age_values.insert(0, "New")
            user_created_age_values = make_list_to_dict(user_created_age_values)
            age_rating_other = multi_choice_Q.question_list_selection(
                user_created_age_values,
                board,
                "Please select a new age classification or an exsisting one",
            )
            if "New" in age_rating_other.get(f"{board}"):
                user_age_value = input("Please enter the boards age value: ")
                age_rating_other[board] = user_age_value
                return age_rating_other, 1
            else:
                return age_rating_other, 0

        else:
            rprint(
                "Age rating - other: has failed to check \
                for 'New' age board."
            )
            # TODO: will error due to not retuning correctly
            return
    else:
        rprint(
            "Age rating - other: has failed to get new data.\n \
            Retuning to Main Menu."
        )
        return


def user_enter_age_rating_other():
    ratings_board = input("Enter the age rating board: ")
    ratings_age = input("Enter the age limit: ")

    return {ratings_board: ratings_age}, 1


# modify for platform other
def platform_other(database_file: str) -> tuple:

    platform_other = Db_search(database_file)

    platform_other_list = platform_other.get_user_created_alt_platforms()

    # Check if there are currently items in the list.
    # If not get a new one.
    if not any(platform_other_list):
        return user_enter_platform_other()

    # If some itmes in the list
    # Insert a placeholer of 'New' and the rest of the list
    elif any(platform_other_list):
        platform_other_list = strip_list(platform_other_list)
        platform_other_list.insert(0, "New")
        platform_other_list = make_list_to_dict(platform_other_list)
        user_choice = multi_choice_Q.question_list_selection(
            platform_other_list, "platform_other", "Select the age platform: "
        )

        # If new selected, get user input of new platform.
        if "New" in user_choice.get("platform_other"):
            return user_enter_platform_other()

        # If not 'New' selected
        elif "New" not in user_choice.get("platform_other"):
            return user_choice.get("platform_other"), 0
    else:
        rprint(
            "Age rating - other: has failed to get new data.\n \
            Retuning to Main Menu."
        )
        return


# modify for platform other
def user_enter_platform_other():
    """Gets the user input for new platform.

    :return: User input for alternate platform. Flag of 1 to show new platform.
    :rtype: tuple
    """
    platform_other = input("Enter the alternative platform: ")

    return platform_other, 1


class Number_of_players:
    """Gets the number of max players per multiplayer type."""

    def __init__(self) -> None:
        pass

    def multiplayer(self, platform_type: str, media: str) -> dict:
        """Takes the platform and gets the online and local multiplayer conut.
        Returns two dictionarys 'online_mulit' and 'local_multi'"""
        n_players = multi_choice_Q.Multiplayer_numbers(platform_type)
        local_multi = n_players.local_mulitplayer(media)
        online_multi = n_players.online_multiplayer(media)

        return online_multi, local_multi

    def multiplayer_coop(self, platform_type: str, media: str) -> dict:
        """Takes the platform and gets the online and local multiplayer conut.
        Returns two dictionarys 'online_mulit' and 'local_multi'"""
        n_players = multi_choice_Q.Multiplayer_numbers(platform_type)
        local_coop = n_players.local_coop(media)
        online_coop = n_players.online_coop(media)

        return online_coop, local_coop
