"""Preping the game_data valiable for insertion into the database"""

from rich import print as rprint


def list_two_values_tuple(item_id: dict, item_value: dict) -> list:
    """Takes two dict values and combines them into a list
    with containing a sets of tuples, each with two values.

    :param item_id: 1st dict values
    :type item_id: dict
    :param item_value: 2nd dict value
    :type item_value: dict
    :return: returns list with sets of tuples each with two values
    :rtype: list
    """
    return [(item_id, items) for items in item_value]


def prep_mode(game_data: dict) -> list:
    """Perpare the game data for insertion into the mode table.

    :param game_data: current list of game data
    :type game_data: dict
    :return: A list containing sets of tuples for each game.
    :rtype: list
    """

    game_id = game_data["game_id"]

    single_player = game_data["game_mode_single"]

    distribution_carrier = "N/A"

    main_list = []

    if "Disc" in game_data.get("medium_of_game"):
        disc = formate_discs(game_data, game_id, single_player, distribution_carrier)
        main_list.extend(disc)

    if "Cartridge" in game_data.get("medium_of_game"):
        cart = formate_cartridge(
            game_data, game_id, single_player, distribution_carrier
        )
        main_list.extend(cart)
    if "Digital" in game_data.get("medium_of_game"):
        digital = formate_digital(
            game_data, game_id, single_player, distribution_carrier
        )
        main_list.extend(digital)

    if "Other" in game_data.get("medium_of_game"):
        other = formate_other(game_data, game_id, single_player, distribution_carrier)
        main_list.extend(other)

    return main_list


def formate_digital(
    game_data: dict,
    game_id: int,
    single_player: int,
    distribution_carrier: str,
) -> list:  # sourcery skip: last-if-guard, remove-unnecessary-else, swap-if-else-branches
    """Perpares the data for the games that are digital.

    :param game_data: current list of game data
    :type game_data: dict
    :param game_id: The ID of the current game
    :type game_id: int
    :param single_player: states if it is a single player or not.
    :type single_player: int
    :return: a list with tuples of all the selected games in the given category
    :rtype: list
    """

    if "Digital" in game_data["medium_of_game"]:
        mode_list_all = []
        barcode = "0"

        for platform in game_data["digital_provider"]:
            if platform not in ["Windows", "Linux", "MacOS"]:
                digital_online_m = (
                    game_data.get("digital_online_multiplayer").get(platform) or "0"
                )
                digital_local_m = (
                    game_data.get("digital_local_multiplayer").get(platform) or "0"
                )
                digital_online_c = (
                    game_data.get("digital_online_coop").get(platform) or "0"
                )
                digital_local_c = (
                    game_data.get("digital_local_coop").get(platform) or "0"
                )

                mode_list = (
                    game_id,
                    platform,
                    distribution_carrier,
                    "Digital",
                    single_player,
                    digital_local_c,
                    digital_online_c,
                    digital_local_m,
                    digital_online_m,
                    barcode,
                )

                mode_list_all.append(
                    mode_list,
                )
            if platform == "Linux":

                for distribution_carrier in game_data["digital_provider_linux"]:
                    digital_online_m = (
                        game_data.get("digital_linux_online_multiplayer").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_local_m = (
                        game_data.get("digital_linux_local_multiplayer").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_online_c = (
                        game_data.get("digital_linux_online_coop").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_local_c = (
                        game_data.get("digital_linux_local_coop").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    mode_list = (
                        game_id,
                        platform,
                        distribution_carrier,
                        "Digital",
                        single_player,
                        digital_local_c,
                        digital_online_c,
                        digital_local_m,
                        digital_online_m,
                        barcode,
                    )

                    mode_list_all.append(
                        mode_list,
                    )
            if platform == "Windows":
                for distribution_carrier in game_data["digital_provider_windows"]:
                    digital_online_m = (
                        game_data.get("digital_windows_online_multiplayer").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_local_m = (
                        game_data.get("digital_windows_local_multiplayer").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_online_c = (
                        game_data.get("digital_windows_online_coop").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_local_c = (
                        game_data.get("digital_windows_local_coop").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    mode_list = (
                        game_id,
                        platform,
                        distribution_carrier,
                        "Digital",
                        single_player,
                        digital_local_c,
                        digital_online_c,
                        digital_local_m,
                        digital_online_m,
                        barcode,
                    )

                    mode_list_all.append(
                        mode_list,
                    )
            if platform == "MacOS":
                for distribution_carrier in game_data["digital_provider_mac"]:
                    digital_online_m = (
                        game_data.get("digital_mac_online_multiplayer").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_local_m = (
                        game_data.get("digital_mac_local_multiplayer").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_online_c = (
                        game_data.get("digital_mac_online_coop").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    digital_local_c = (
                        game_data.get("digital_mac_local_coop").get(
                            distribution_carrier
                        )
                        or "0"
                    )
                    mode_list = (
                        game_id,
                        platform,
                        distribution_carrier,
                        "Digital",
                        single_player,
                        digital_local_c,
                        digital_online_c,
                        digital_local_m,
                        digital_online_m,
                        barcode,
                    )

                    mode_list_all.append(
                        mode_list,
                    )
        return mode_list_all
    else:
        return


def formate_cartridge(
    game_data: dict,
    game_id: int,
    single_player: int,
    distribution_carrier: str,
) -> list:
    """Perpares the data for the games that are on cartridge.

    :param game_data: current list of game data
    :type game_data: dict
    :param game_id: The ID of the current game
    :type game_id: int
    :param single_player: states if it is a single player or not.
    :type single_player: int
    :return: a list with tuples of all the selected games in the given category
    :rtype: list
    """

    if "Cartridge" in game_data["medium_of_game"]:
        mode_list_all = []
        for platform in game_data["platform_physical_cartridge"]:
            cart_online_m = (
                game_data.get("cartridge_online_multiplayer").get(platform) or "0"
            )
            cart_local_m = (
                game_data.get("cartridge_local_muliplayer").get(platform) or "0"
            )
            cart_online_c = game_data.get("cartridge_online_coop").get(platform) or "0"
            cart_local_c = game_data.get("cartridge_local_coop").get(platform) or "0"

            barcode = (
                game_data.get("platform_physical_cartridge_barcodes").get(platform)
                or "0"
            )

            mode_list = (
                game_id,
                platform,
                distribution_carrier,
                "Cartridge",
                single_player,
                cart_local_c,
                cart_online_c,
                cart_local_m,
                cart_online_m,
                barcode,
            )

            mode_list_all.append(
                mode_list,
            )

        return mode_list_all
    else:
        return


def formate_discs(
    game_data: dict,
    game_id: int,
    single_player: int,
    distribution_carrier: str,
) -> list:
    """Perpares the data for the games that are on discs.

    :param game_data: current list of game data
    :type game_data: dict
    :param game_id: The ID of the current game
    :type game_id: int
    :param single_player: states if it is a single player or not.
    :type single_player: int
    :return: a list with tuples of all the selected games in the given category
    :rtype: list
    """
    if "Disc" in game_data["medium_of_game"]:
        mode_list_all = []
        for platform in game_data["platform_physical_disc"]:
            disc_online_m = (
                game_data.get("disc_online_multiplayer").get(platform) or "0"
            )

            disc_local_m = game_data.get("disc_local_multiplayer").get(platform) or "0"
            disc_online_c = game_data.get("disc_online_coop").get(platform) or "0"
            disc_local_c = game_data.get("disc_local_coop").get(platform) or "0"
            barcode = (
                game_data.get("platform_physical_disc_barcodes").get(platform) or "0"
            )

            mode_list = (
                game_id,
                platform,
                distribution_carrier,
                "Disc",
                single_player,
                disc_local_c,
                disc_online_c,
                disc_local_m,
                disc_online_m,
                barcode,
            )

            mode_list_all.append(
                mode_list,
            )
        return mode_list_all
    else:
        return


def formate_other(
    game_data: dict,
    game_id: int,
    single_player: int,
    distribution_carrier: str,
) -> list:
    """Perpares the data for the games that are on other platforms.

    :param game_data: current list of game data
    :type game_data: dict
    :param game_id: The ID of the current game
    :type game_id: int
    :param single_player: states if it is a single player or not.
    :type single_player: int
    :return: a list with tuples of all the selected games in the given category
    :rtype: list
    """
    disc_online_m = game_data.get("platform_other_online_multiplayer") or "0"

    disc_local_m = game_data.get("platform_other_local_multiplayer") or "0"
    disc_online_c = game_data.get("platform_other_online_coop") or "0"
    disc_local_c = game_data.get("platform_other_local_coop") or "0"
    barcode = game_data.get("platform_other_barcodes") or "0"

    platform = game_data.get("platform_other") or "0"

    mode_list = (
        game_id,
        platform,
        distribution_carrier,
        "Disc",
        single_player,
        disc_local_c,
        disc_online_c,
        disc_local_m,
        disc_online_m,
        barcode,
    )

    return [mode_list]


def prep_age_rating(game_data: dict) -> tuple:
    """Formates the age rating into seperate list
    ready to insert into the database.

    :param game_data: Dictionary of the current game data
    :type game_data: dict
    :return: Returns two lists of the rating board and the value.
    example [ESRB], ["3+"]
    :rtype: tuple of two lists
    """
    age_rating_board = list(game_data["age_rating"].keys())
    age_rating_board = age_rating_board[0]

    age_rating_grade = list(game_data["age_rating"].values())
    age_rating_grade = age_rating_grade[0]

    return age_rating_board, age_rating_grade
