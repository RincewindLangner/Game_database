# Data collection

import source_code.db_multi_choice_questions as multi_choice_Q
from source_code.main_program import (
    Number_of_players,
    age_rating_from_selected_board,
    age_rating_other,
    barcode,
    dictionary_default,
    eshop_os_platform,
    game_eshop_OS,
    make_dict_to_list,
    platform_other,
)
from source_code.search import Db_search
from rich import print as rprint


def data_collect(database_file: str) -> dict:
    """The collection of the users game data
    ready for input into the database"""

    print("Please now enter your games data.\n")

    game_data = dictionary_default()

    title = str(input("Title of the game: "))

    description = input("Please enter the games description: ")

    publisher = input("Publisher: ")

    developer = input("Developer: ")

    game_data.update({"title": title})
    game_data.update({"description": description})
    game_data.update({"publisher": publisher})
    game_data.update({"developer": developer})

    platform_medium = {"medium_of_game": ""}
    while True:
        if any(platform_medium.values()) is False:
            platform_medium = multi_choice_Q.medium()
        else:
            break

    # adding two dictonarys together
    game_data = game_data | platform_medium

    # TODO: my need a while loop to make sure an answer is got.
    for item_medium in game_data["medium_of_game"]:
        if item_medium == "Disc":
            while True:
                if any(game_data.get("platform_physical_disc")) is False:
                    platform_quary_on_disc = multi_choice_Q.physical_disc()
                    game_data |= platform_quary_on_disc
                else:
                    break

            # Get barcode
            game_data = barcode(game_data, "platform_physical_disc")

        if item_medium == "Cartridge":
            while True:
                if any(game_data.get("platform_physical_cartridge")) is False:
                    platform_quary_on_cartridge = multi_choice_Q.physical_cartridge()
                    game_data |= platform_quary_on_cartridge
                else:
                    break

            # Get barcode
            game_data = barcode(game_data, "platform_physical_cartridge")

        if item_medium == "Digital":
            while True:
                if any(game_data.get("digital_provider")) is False:
                    platform_quary_on_digital = multi_choice_Q.digital_provider()
                    game_data |= platform_quary_on_digital
                else:
                    break

            platform_digital = make_dict_to_list(platform_quary_on_digital)
            whitch_platform_os = game_eshop_OS(platform_digital)
            game_data = eshop_os_platform(game_data, whitch_platform_os)

        if item_medium == "Other":
            alternative_platform, alternative_platform_flag = platform_other(
                database_file
            )
            game_data.update({"platform_other": alternative_platform})
            game_data.update({"platform_other_user_created": alternative_platform_flag})

    age_rating = age_rating_from_selected_board()

    if age_rating == "other":
        age_rating, new_user_age_rating_flag = age_rating_other(database_file)
        game_data.update({"age_rating_user_created": new_user_age_rating_flag})

    game_data.update({"age_rating": age_rating})

    game_modes = {"number_of_player": None}

    while True:
        if any(game_modes.values()) is False:
            game_modes = multi_choice_Q.game_modes()
        else:
            break

    game_modes = make_dict_to_list(game_modes)

    if "Single player" in game_modes:
        game_data.update({"game_mode_single": 1})

    # TODO: not got other platforms like xbox or ps3 number of players

    if "Mulitplayer" in game_modes:
        game_data.update({"game_mode_multiplayer": 1})

        n_multiplayer = Number_of_players()

        # TODO: get an if stament to check if
        # they are all the same and auto fill?

        # TODO: get online into collection in a dict,
        # get local into a collection in a dict

        if game_data["digital_provider"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["digital_provider"]:
                online_n, local_n = n_multiplayer.multiplayer(item, "Digital")
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["digital_online_multiplayer"] = online_multiplayer_n
            game_data["digital_local_multiplayer"] = local_multiplayer_n

        if game_data["platform_physical_disc"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["platform_physical_disc"]:
                online_n, local_n = n_multiplayer.multiplayer(item, "Disc")
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["disc_online_multiplayer"] = online_multiplayer_n
            game_data["disc_local_multiplayer"] = local_multiplayer_n

        if game_data["platform_physical_cartridge"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["platform_physical_cartridge"]:
                online_n, local_n = n_multiplayer.multiplayer(item, "Cartridge")
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["cartridge_online_multiplayer"] = online_multiplayer_n
            game_data["cartridge_local_muliplayer"] = local_multiplayer_n

        if game_data["digital_provider_windows"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["digital_provider_windows"]:
                online_n, local_n = n_multiplayer.multiplayer(
                    item, "Digital Windows provider"
                )
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["digital_windows_online_multiplayer"] = online_multiplayer_n
            game_data["digital_windows_local_multiplayer"] = local_multiplayer_n

        if game_data["digital_provider_linux"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["digital_provider_linux"]:
                online_n, local_n = n_multiplayer.multiplayer(
                    item, "Digital Linux provider"
                )
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["digital_linux_online_multiplayer"] = online_multiplayer_n
            game_data["digital_linux_local_multiplayer"] = local_multiplayer_n

        if game_data["digital_provider_mac"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["digital_provider_mac"]:
                online_n, local_n = n_multiplayer.multiplayer(
                    item, "Digital Mac provider"
                )
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["digital_mac_online_multiplayer"] = online_multiplayer_n
            game_data["digital_mac_local_multiplayer"] = local_multiplayer_n

        # TODO: need to add the option for the 'Other' platform

    if "Co-op" in game_modes:
        game_data.update({"game_mode_coop": 1})

        n_multiplayer = Number_of_players()

        if game_data["digital_provider"] != "":
            online_multiplayer_n = dict()
            local_multiplayer_n = dict()
            for item in game_data["digital_provider"]:
                online_n, local_n = n_multiplayer.multiplayer_coop(item, "Digital")
                online_multiplayer_n |= online_n
                local_multiplayer_n |= local_n

            game_data["digital_online_coop"] = online_multiplayer_n
            game_data["digital_local_coop"] = local_multiplayer_n

        if game_data["platform_physical_disc"] != "":
            online_coop_n = dict()
            local_coop_n = dict()
            for item in game_data["platform_physical_disc"]:
                online_n, local_n = n_multiplayer.multiplayer_coop(item, "Disc")
                online_coop_n |= online_n
                local_coop_n |= local_n

            game_data["disc_online_coop"] = online_coop_n
            game_data["disc_local_coop"] = local_coop_n

        if game_data["platform_physical_cartridge"] != "":
            online_coop_n = dict()
            local_coop_n = dict()
            for item in game_data["platform_physical_cartridge"]:
                online_n, local_n = n_multiplayer.multiplayer_coop(item, "Cartridge")
                online_coop_n |= online_n
                local_coop_n |= local_n

            game_data["cartridge_online_coop"] = online_coop_n
            game_data["cartridge_local_coop"] = local_coop_n

        if game_data["digital_provider_windows"] != "":
            online_coop_n = dict()
            local_coop_n = dict()
            for item in game_data["digital_provider_windows"]:
                online_n, local_n = n_multiplayer.multiplayer_coop(
                    item, "Digital Windows provider"
                )
                online_coop_n |= online_n
                local_coop_n |= local_n

            game_data["digital_windows_online_coop"] = online_coop_n
            game_data["digital_windows_local_coop"] = local_coop_n

        if game_data["digital_provider_linux"] != "":
            online_coop_n = dict()
            local_coop_n = dict()
            for item in game_data["digital_provider_linux"]:
                online_n, local_n = n_multiplayer.multiplayer_coop(
                    item, "Digital Lunix provider"
                )
                online_coop_n |= online_n
                local_coop_n |= local_n

            game_data["digital_linux_online_coop"] = online_coop_n
            game_data["digital_linux_local_coop"] = local_coop_n

        if game_data["digital_provider_mac"] != "":
            online_coop_n = dict()
            local_coop_n = dict()
            for item in game_data["digital_provider_mac"]:
                online_n, local_n = n_multiplayer.multiplayer_coop(
                    item, "Digital Mac provider"
                )
                online_coop_n |= online_n
                local_coop_n |= local_n

            game_data["digital_mac_online_coop"] = online_coop_n
            game_data["digital_mac_local_coop"] = local_coop_n

        # TODO: need to add the option for the 'Other' platform

    while True:
        if not any(game_data["theme"]):
            theme = multi_choice_Q.themes()
            if theme.get("theme") == []:
                continue
        if theme.get("theme") != []:
            game_data.update({"theme": theme.get("theme")})
            break
        else:
            break

    while True:
        if not any(game_data["genres"]):
            genre = multi_choice_Q.genres()
            if genre.get("genres") == []:
                continue
        if genre.get("genres") != []:
            game_data.update({"genres": genre.get("genres")})
            break
        else:
            break

    user_input = input("What game series is it? ").strip()

    game_data = series_quary_user_list(database_file, game_data, user_input)

    return game_data


def series_quary_user_list(
    database_file: str, game_data: dict, user_input: str
) -> dict:
    quary = Db_search(database_file)
    user_input_to_dict = {"0": user_input}
    game_series = quary.series_quary(user_input_to_dict)
    game_series = list(game_series.values())
    game_data["series"] = game_series

    return game_data
