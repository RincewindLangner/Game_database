"""Uses the data to import into the database."""
import source_code.db_connection_control as db_c_c
import source_code.insertion_prep as insert_prep

from rich import print as rprint


def db_insert(db_file_name: str, game_data: dict):
    """Takes the file name and the data to enter into the database."""
    conn, c = db_c_c.db_connect(db_file_name)
    try:
        insert_sql(conn, c, game_data)
    except Exception:
        print("Error inserting the data into the database")
        conn.rollback()

    finally:
        conn.close()
    return


def insert_sql(conn, c, game_data: dict):
    """Inserts the current game data into the database.

    :param conn: Sqlite connection
    :type conn: ?
    :param c: Sqlite connection
    :type c: ?
    :param game_data: Dictonary of current game data.
    :type game_data: dict
    """
    c.execute("""SELECT max(game_id) FROM games""")
    max_game_id = c.fetchone()
    if None in max_game_id:
        max_game_id = 1
    else:
        max_game_id = max_game_id[0]
        max_game_id += 1
    game_data["game_id"] = max_game_id

    # Base data for game
    c.execute(
        "INSERT INTO games (game_id, title, description, publisher, \
            developer) VALUES (:game_id, :title, :description,\
                :publisher, :developer)",
        game_data,
    )

    # Genres:
    data_value = insert_prep.list_two_values_tuple(
        game_data["game_id"], game_data["genres"]
    )

    c.executemany(
        "INSERT INTO genre (game_id, genre) VALUES \
                (?, ?)",
        data_value,
    )

    # Theme:
    data_value = insert_prep.list_two_values_tuple(
        game_data["game_id"], game_data["theme"]
    )

    c.executemany(
        "INSERT INTO theme (game_id, theme) VALUES \
                (?, ?)",
        data_value,
    )

    # Series
    c.execute(
        "SELECT series FROM game_series \
                WHERE series = (?)",
        game_data["series"],
    )

    series_name = c.fetchone()

    if series_name is None:
        c.execute(
            "INSERT INTO game_series (series) VALUES \
                    (?)",
            game_data["series"],
        )
    c.execute(
        "SELECT key_id FROM game_series \
                    WHERE series = (?)",
        game_data["series"],
    )
    series_key_id = c.fetchone()
    series_key_id = series_key_id[0]

    c.execute(
        "INSERT INTO game_series_link (game_id, series) VALUES \
                (?, ?)",
        [game_data["game_id"], series_key_id],
    )

    # Mode
    game_values = insert_prep.prep_mode(game_data)

    c.executemany(
        "INSERT INTO mode (game_id, platform, distribution_carrier, medium, \
                single_player, offline_coop_max, online_coop_max, \
                    offline_multiplayer_max, online_multiplayer_max, barcode \
                        ) VALUES (?, ?, ?, ?, ?, ?, \
                            ?, ?, ?, ?)",
        game_values,
    )

    # age rating

    age_rating_board, age_rating_grade = insert_prep.prep_age_rating(game_data)

    c.execute(
        "INSERT INTO age_rating (game_id, board, rating) VALUES \
                (?, ?, ?)",
        [game_data["game_id"], age_rating_board, age_rating_grade],
    )

    # age rating user creation

    if game_data.get("age_rating_user_created") == 1:

        board, age = insert_prep.prep_age_rating(game_data)

        c.execute(
            "INSERT INTO age_rating_user_list (board, rating) VALUES \
                    (?, ?)",
            [board, age],
        )

    # platform_other
    if game_data.get("platform_other_user_created") == 1:
        c.execute(
            "INSERT INTO platform_other (platform_other) \
                VALUES (:platform_other)",
            game_data,
        )

    print("Game's data has been added to the database.")

    conn.commit()

    return
