import source_code.db_connection_control as db_c_c


def create_db_file(db_file_name: str) -> None:
    """Pass a new file name complete with .sqlite3 extention.
    A new file and database tables will be
    created in the db_save_files folder."""
    # turn on foreign key support everytime you connect.
    conn, c = db_c_c.db_connect(db_file_name)
    try:

        # table creation
        c.executescript(
            """
        CREATE TABLE IF NOT EXISTS "age_rating" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "board"     TEXT,
            "rating"    TEXT,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "age_rating_user_list" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "board"     TEXT,
            "rating"    TEXT
        );
        CREATE TABLE IF NOT EXISTS "games" (
            "game_id"   INTEGER NOT NULL,
            "title"     TEXT,
            "description"    TEXT,
            "publisher" TEXT,
            "developer" TEXT,
            PRIMARY KEY("game_id" AUTOINCREMENT)
        );
        CREATE TABLE IF NOT EXISTS "game_series" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "series"    TEXT
        );
        CREATE TABLE IF NOT EXISTS "game_series_link" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "series"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id"),
            FOREIGN KEY("series") REFERENCES "game_series"("key_id")
        );
        CREATE TABLE IF NOT EXISTS "genre" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "genre"     TEXT,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "mode" (
            "key_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"	INTEGER,
            "platform"	TEXT,
            "distribution_carrier"  TEXT,
            "medium"	TEXT,
            "single_player"	INTEGER,
            "offline_coop_max"	TEXT,
            "online_coop_max"	TEXT,
            "offline_multiplayer_max"	TEXT,
            "online_multiplayer_max"	TEXT,
            "barcode"   TEXT,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "platform_other" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "platform_other"    TEXT
        );
        CREATE TABLE IF NOT EXISTS "theme" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "theme"     TEXT,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
            """
        )

        print(f"Database has been created called {db_file_name}")

    except Exception:
        # Roll back if fails
        db_c_c.db_rollback(conn)
    # TODO: add a retry option in the final version

    finally:
        db_c_c.db_commit_close(conn)
        return
