# Questions for getting the data for the database.
# https://pypi.org/project/inquirer/

import inquirer
import source_code.question_variables as question_variables


def main_menu() -> dict:
    """Select what option you need to start the program."""
    question = [
        inquirer.List(
            "main_menu",
            message="Main menu",
            choices=question_variables.values_menu(),
        ),
    ]
    answer = inquirer.prompt(question)

    answer = answer.get("main_menu")

    return answer


def menu_open_options() -> dict:
    """Select where you want to search the database."""
    question = [
        inquirer.List(
            "search",
            message="Select an option",
            choices=question_variables.values_menu_open_options(),
        )
    ]
    answer = inquirer.prompt(question)

    answer = answer.get("search")

    return answer


def medium() -> dict:
    """Lets you select what media the game is on."""
    questions = [
        inquirer.Checkbox(
            "medium_of_game",
            message="Please select the medium type: ",
            choices=question_variables.values_medium("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def physical_disc() -> dict:
    """Lets you select what platform the game is on."""
    questions = [
        inquirer.Checkbox(
            "platform_physical_disc",
            message="Disc - Please select the platform",
            choices=question_variables.values_physical_disc("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def physical_cartridge() -> dict:
    """Lets you select what platform the game is on."""
    questions = [
        inquirer.Checkbox(
            "platform_physical_cartridge",
            message="Cartridge - Please select the platform",
            choices=question_variables.values_physical_cart("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def digital_provider() -> dict:
    """Lets you select what platform the game is on."""
    questions = [
        inquirer.Checkbox(
            "digital_provider",
            message="Digital - Please select the platform it is downloaded on: ",
            choices=question_variables.values_digital_provider("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def sofware_digital_provider_windows() -> dict:
    """Lets you select what platform the game is on."""
    questions = [
        inquirer.Checkbox(
            "digital_provider_windows",
            message="Please select the which digital provider on Windows: ",
            choices=question_variables.values_digital_provider_windows("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def digital_provider_linux() -> dict:
    """Lets you select what platform the game is on."""
    questions = [
        inquirer.Checkbox(
            "digital_provider_linux",
            message="Please select the which digital provider on Linux: ",
            choices=question_variables.values_digital_provider_linux("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def digital_provider_mac() -> dict:
    """Lets you select what platform the game is on."""
    questions = [
        inquirer.Checkbox(
            "digital_provider_mac",
            message="Please select the which digital provider on MacOS: ",
            choices=question_variables.values_digital_provider_mac("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def themes() -> dict:
    """What theme is the game."""
    questions = [
        inquirer.Checkbox(
            "theme",
            message="Theme - Select all that apply: ",
            choices=question_variables.values_theme("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(questions)


def genres() -> dict:
    """What genre is it."""
    question = [
        inquirer.Checkbox(
            "genres",
            message="Genres - Select all that apply: ",
            choices=question_variables.values_genres("qa"),
            default=None,
        )
    ]

    return inquirer.prompt(question)


def yes_no_option(text_display: str) -> bool:
    """The user answers 'yes' or 'no', Returns answer as a True if yes.
    If no value is entered returns True as default.
    Must have the text you want to display for the question
    in the attribute when calling the function."""
    question = [
        inquirer.Confirm("yes_or_no", message=f"{text_display}", default=True),
    ]
    answer = inquirer.prompt(question)

    answer = answer.get("yes_or_no")

    return answer


def get_age_rating_board() -> dict:
    """Select what age classification you want to use."""
    questions = [
        inquirer.List(
            "age_class",
            message="Which age classifications do you want to use: ",
            choices=question_variables.values_age_rating_board("qa"),
        )
    ]

    answer = inquirer.prompt(questions)

    answer = answer.get("age_class")

    return answer


def age_rate_elspa():
    """Age rating for Entertainment and
    Leisure Software Publishers Association."""
    questions = [
        inquirer.List(
            "ELSPA",
            message="Select the highest Entertainment and Leisure Software Publishers Association (ELSPA) rating for the game: ",
            choices=question_variables.values_age_rate_elspa("qa"),
        )
    ]

    return inquirer.prompt(questions)


def age_rate_pegi():
    """Age rating for PEGI"""
    questions = [
        inquirer.List(
            "PEGI",
            message="Select the highest PEGI rating for the game: ",
            choices=question_variables.values_age_rate_pegi("qa"),
        )
    ]

    return inquirer.prompt(questions)


def age_rate_esrb():
    """Age rating for ESRB"""
    questions = [
        inquirer.List(
            "ESRB",
            message="Select the highest ESRB rating for the game: ",
            choices=question_variables.values_age_rate_esrb("qa"),
        )
    ]

    return inquirer.prompt(questions)


def age_rate_app_store():
    """Age rating for the App store"""
    questions = [
        inquirer.List(
            "App store",
            message="Select the highest App store rating for the game: ",
            choices=question_variables.values_age_rate_app_store("qa"),
        )
    ]

    return inquirer.prompt(questions)


def age_rate_aus():
    """Age rating for Australian classification board"""
    questions = [
        inquirer.List(
            "Australian classification board (ACB)",
            message="Select the highest Australian classification board rating for the game: ",
            choices=question_variables.values_age_rate_aus("qa"),
        )
    ]

    return inquirer.prompt(questions)


def age_rate_cero():
    """Age rating for CERO - Japan."""
    questions = [
        inquirer.List(
            "CERO",
            message="Select the highest Computer Entertainment Rating Organisation rating, Japan, for the game: ",
            choices=question_variables.values_age_rate_cero("qa"),
        )
    ]

    return inquirer.prompt(questions)


def age_rate_bbfc():
    """Age rating for The British media rating."""
    questions = [
        inquirer.List(
            "BBFC",
            message="Select the highest The British Board of file classification rating for the game: ",
            choices=question_variables.values_age_rate_bbfc("qa"),
        )
    ]

    return inquirer.prompt(questions)


# TODO working on
def age_rating_user_list(boards_name: list) -> dict:
    """Asked the user which user created rating board they want to use.
    Need to insert "new" at the start of the list, enabling the option of
    adding a new user selection.

    :param board_name: User input rating board. Need to insert "new"
    at the start of the list, enabling the option of adding a new user
    selection.
    :type board_name: list
    :return: Returns dictionary of users choice. {"age_board_other": "..."}
    :rtype: Dictonairy
    """
    question = [
        inquirer.List(
            "age_board_other",
            message="Select the age rating board: ",
            choices=boards_name,
        )
    ]

    return inquirer.prompt(question)


def game_modes():
    """Select if a game has Single, Mulitplayer or both"""
    question = [
        inquirer.Checkbox(
            "number_of_player",
            message="Please select if it is a Single and or multipayer game: ",
            choices=question_variables.values_game_modes(),
        )
    ]

    return inquirer.prompt(question)


def question_list_selection(current_list: dict, list_name: str, message: str) -> dict:
    """Makes a selection list that returns one dictonary value from the user.

    :example:
    \n> Option one
    \n> Option two
    \n> ...

    :param list: The dictonary list from which the \
        user selection list is created.
    :type list: dict
    :param list_name: Name of the question and the key in the dict
    :type list_name: str
    :param message: Messaged to as the user
    :type message: str
    :return: Key:value pair made from the list and user selction.
    :rtype: dict
    """
    questions = [
        inquirer.List(
            f"{list_name}",
            message=f"{message}",
            choices=lambda x: current_list.values(),
        )
    ]
    return inquirer.prompt(questions)


class Multiplayer_numbers:
    """Multiplayer for a single platform."""

    def __init__(self, platfrom) -> None:
        self.plaform = platfrom

    def local_coop(self, media: str) -> dict:
        """Select how many players for local co-op play."""
        question = [
            inquirer.List(
                f"{self.plaform}",
                message=f"[{media}] {self.plaform} - Co-op, local?",
                choices=question_variables.values_player_numbers_0_7(),
            )
        ]

        return inquirer.prompt(question)

    def online_coop(self, media: str) -> dict:
        """Select how many players for online co-op play."""
        question = [
            inquirer.List(
                f"{self.plaform}",
                message=f"[{media}] {self.plaform} - Co-op, online?",
                choices=question_variables.values_player_numbers_0_7(),
            )
        ]

        return inquirer.prompt(question)

    def local_mulitplayer(self, media: str) -> dict:
        """Select how many players for LAN / local multiplayer play."""
        question = [
            inquirer.List(
                f"{self.plaform}",
                message=f"[{media}] {self.plaform} - Multiplayer, LAN / local?",
                choices=question_variables.values_player_numbers_0_7(),
            )
        ]

        return inquirer.prompt(question)

    def online_multiplayer(self, media: str) -> dict:
        """Select how many players for online multiplayer."""
        question = [
            inquirer.List(
                f"{self.plaform}",
                message=f"[{media}] {self.plaform} - Multiplayer, online?",
                choices=question_variables.values_player_numbers_0_7(),
            )
        ]

        return inquirer.prompt(question)
