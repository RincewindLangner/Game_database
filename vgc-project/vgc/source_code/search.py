# Searching and retrival of the database.
from rich import print as rprint

from source_code.db_connection_control import db_commit_close, db_connect
from source_code.db_multi_choice_questions import question_list_selection

# from source_code.main_program import make_dict_from_list


class Db_search:
    """Search and retrival of the game database"""

    def __init__(self, database_file: str):
        self.database_file = database_file

    def db_retrive_one_game(self, game_id: int):
        """
        Retrive one game from the database using the game_id

        :param game_id: Game id
        :type game_id: int
        :return: _description_
        :rtype: _type_
        """
        # TODO: not working yet.
        conn, c = db_connect(self.database_file)
        c.execute(f"SELECT * FROM games WHERE game_id = {game_id}")
        selection = c.fetchall()

        db_commit_close(conn)

        return selection

    def db_retive_n_of_game_tiles_acending(
        self, offest: int = 0, nlimit: int = -1
    ) -> list:
        """Gets a set number of game titles in acending order.

        :param offest: Number ot offset it by.
        :type offest: Integer
        :param nlimit: Number of how many to select.
        :type nlimit: Integer
        :return: Returns a tuple list
        :rtype: list[(tuple,) ...]
        """
        conn, c = db_connect(self.database_file)
        c.execute(
            f"SELECT title FROM games ORDER BY  title ASC LIMIT {nlimit} OFFSET {offest}"
        )
        selection = c.fetchall()

        db_commit_close(conn)

        return selection

    def series_quary(self, user_input: dict) -> dict:
        """Search the datase for existing game
        series and display them back to the user."""
        conn, c = db_connect(self.database_file)
        c.execute("""SELECT * FROM game_series""")
        selection = c.fetchall()
        selection = dict(selection)
        user_input |= selection
        user_selection = question_list_selection(
            user_input,
            "series",
            "Comfirm new series (top) or select from the exesiting ones",
        )

        db_commit_close(conn)

        return user_selection

    def get_user_created_alt_platforms(self) -> list:
        """Queries the database to get the user created list of
        alternative platforms.

        :return: list of tuples < [(item,), (item2,), ...] >
        :rtype: list
        """
        conn, c = db_connect(self.database_file)
        c.execute("SELECT DISTINCT platform_other FROM platform_other")
        selection = c.fetchall()

        db_commit_close(conn)

        return selection

    # def user_platform(self, user_input: dict) -> dict:
    #     """Search the datase for existing game
    #     series and display them back to the user."""
    #     conn, c = db_connect(self.database_file)
    #     c.execute("SELECT DISTINCT platform_other FROM platform_other")
    #     selection = c.fetchall()
    #     selection = dict(selection)
    #     user_input |= selection
    #     user_selection = question_list_selection(
    #         user_input,
    #         "platform_other",
    #         "Comfirm new platform (top) or select from the exesiting ones",
    #     )

    #     db_commit_close(conn)

    #     return user_selection

    def get_user_created_age_boards(self) -> list:
        """Queries the database to get the user created list of
        age rating boards

        :return: list of tuples < [(item,), (item2,), ...] >
        :rtype: list
        """
        conn, c = db_connect(self.database_file)
        c.execute("SELECT DISTINCT board FROM age_rating_user_list")
        selection = c.fetchall()

        db_commit_close(conn)

        return selection

    def get_user_created_age_values(self, age_board: str) -> list:
        """
        Selects and return a distince list of user created age ratings

        :param age_board: The age rating board to get the age ratings
        :type age_board: str
        :return: List of all the current distinct age rating levels
        :rtype: list
        """
        conn, c = db_connect(self.database_file)
        c.execute(
            f"SELECT DISTINCT rating FROM age_rating_user_list\
             WHERE board = '{age_board}'"
        )
        selection = c.fetchall()

        db_commit_close(conn)

        return selection
