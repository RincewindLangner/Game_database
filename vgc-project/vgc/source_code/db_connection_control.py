"""Contains the connection, commit and close of the database."""

from pathlib import Path
import sqlite3


def db_connect(db_file_name: str) -> str:
    """Connect to a database. Returns conn, c"""
    f_name = Path("db_save_files", db_file_name)
    f_name = f_name.resolve()
    conn = sqlite3.connect(f_name)
    conn.execute("PRAGMA foreign_keys = ON")
    c = conn.cursor()
    return conn, c


def db_commit_close(conn):
    """Commits and closes the database."""
    try:
        conn.commit()
        conn.close()
        return
    except Exception:
        db_rollback(conn)
        print("Failed to create database.")
        return


def db_rollback(conn):
    """Rollback and closes the database."""
    conn.rollback()
    print("Database failed to complete the task.")
    return
