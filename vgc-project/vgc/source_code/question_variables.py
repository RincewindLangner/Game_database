"""Holds all the variables and values for the questions."""


def values_menu() -> list:
    """Returns [string], in square brackects,
    of options for use in the inquirer choises menu."""

    return ["New", "Open", "Exit"]


def values_menu_open_options() -> list:
    """Returns [string], in square brackects,
    of options for use in the inquirer choises menu."""

    return ["Search", "Add", "Edit", "Delete", "Exit"]


def strip_list_for_qa(var: list) -> list:
    """Strips extra charactures from the tuple list.
    Converting them to a standard list.

    :param var: List containing sets of tuples.
    :type var: list
    :return: Standard list.
    :rtype: list
    """
    j = []
    for x in var:
        x = str(x)
        j.append(x.strip("\",(')"))
    return j


def values_medium(state: str) -> list:
    """'qa' or 'db'. Returns [string], in square brackects,
    of options for use in the inquirer choises menu."""

    var = [
        ("Disc",),
        ("Cartridge",),
        ("Digital",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_physical_disc(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Mega-CD",),
        ("Sega Saturn",),
        ("Dreamcast",),
        ("Xbox OG",),
        ("Xbox 360",),
        ("Xbox ONE X/S",),
        ("Xbox Series X/S",),
        ("PS1",),
        ("PS2",),
        ("PS3",),
        ("PS4 /pro/",),
        ("PS5",),
        ("PSP",),
        ("Gamecube",),
        ("Wii",),
        ("Wii U",),
        ("Windows - Disc",),
        ("Linux - Disc",),
        ("MacOS - Disc",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_physical_cart(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Master system",),
        ("Mega Drive",),
        ("Gameboy",),
        ("Gameboy colour",),
        ("Gameboy advanced",),
        ("DS",),
        ("3DS",),
        ("NES",),
        ("SNES",),
        ("N64",),
        ("Switch",),
        ("PS Vita",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_digital_provider(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Xbox 360",),
        ("Xbox ONE X/S",),
        ("Xbox series X/S",),
        ("PS3",),
        ("PS4",),
        ("PS5",),
        ("PSP",),
        ("PS Vita",),
        ("DSi",),
        ("3DS",),
        ("Wii",),
        ("Wii U",),
        ("Switch",),
        ("Windows",),
        ("Linux",),
        ("MacOS",),
        ("Google Stadia",),
        ("Nvidia shield",),
        ("Andriod",),
        ("iOS",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_digital_provider_windows(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Steam",),
        ("GOG",),
        ("Epic",),
        ("Twitch / Amazon games",),
        ("Windows store",),
        ("Humble Bundle",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_digital_provider_linux(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Steam",),
        ("GOG",),
        ("Humble Bundle",),
        ("Software manager",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_digital_provider_mac(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Steam",),
        ("GOG",),
        ("Humble Bundle",),
        ("App store",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_theme(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("N/A",),
        ("4x (explore, expand, exploit and exterminate)",),
        ("Action",),
        ("Business",),
        ("Comedy",),
        ("Drama",),
        ("Education",),
        ("Erotic",),
        ("Fantasy",),
        ("Historical",),
        ("Horror",),
        ("Kids",),
        ("Mystery",),
        ("Non-fiction",),
        ("Open world",),
        ("Party ",),
        ("Romance",),
        ("Sandbox",),
        ("Science fiction",),
        ("Stealth",),
        ("Survival",),
        ("Thriller",),
        ("Warfare",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_genres(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("N/A",),
        ("Adventure",),
        ("Arcade",),
        ("Card and board game",),
        ("Fighting",),
        ("Hack and slash / Beat 'em up",),
        ("Indie",),
        ("MOBA",),
        ("Music",),
        ("Pinball",),
        ("Platform",),
        ("Point-and-click",),
        ("Puzzel",),
        ("Quiz / Trivia",),
        ("Racing",),
        ("Real time strategy",),
        ("Role-playing",),
        ("Shooter",),
        ("Simulator",),
        ("Sport",),
        ("Strategy",),
        ("Tactical",),
        ("Turn-based strategy",),
        ("Visual novel",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rating_board(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("PEGI",),
        ("ESRB",),
        ("ELSPA",),
        ("BBFC",),
        ("CERO",),
        ("App store",),
        ("Australian classification board /ACB/",),
        ("Not rated",),
        ("Other",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_elspa(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("3+",),
        ("11+",),
        ("15+",),
        ("18+",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_pegi(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("3",),
        ("7",),
        ("12",),
        ("16",),
        ("18",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_esrb(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("RP - Rating pending",),
        ("EC - Early childhood",),
        ("E - Everyone",),
        ("E10+ - Everyone 10+",),
        ("T - Teen",),
        ("M - Mature",),
        ("A - Adult",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_app_store(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("4+",),
        ("9+",),
        ("12+",),
        ("17+",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_aus(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("G - General",),
        ("PG - Parental guidance",),
        ("M - Mature",),
        ("MA15+ - Restricted",),
        ("R18+ - Restricted",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_cero(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("A: All ages",),
        ("B: from age 12",),
        ("C: from age 15",),
        ("D: from age 17. Discretion is advised",),
        ("Z: from age 18",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_age_rate_bbfc(state: str) -> list:
    """
    db = for the database
    qa = for the questions

    Returns [list]"""

    var = [
        ("Uc - Universal children",),
        ("U - Universal",),
        ("PG - Parental guidance",),
        ("12A",),
        ("12",),
        ("15",),
        ("18",),
    ]

    if state == "db":
        return var

    if state == "qa":
        return strip_list_for_qa(var)


def values_game_modes() -> list:
    """Returns [string], in square brackects,
    of options for use in the inquirer choises menu."""

    return ["Single player", "Mulitplayer", "Co-op"]


def values_player_numbers_0_7() -> list:
    """Returns [string], in square brackects,
    of options for use in the inquirer choises menu."""

    return ["0", "2", "3", "4", "5", "6", "7+"]
