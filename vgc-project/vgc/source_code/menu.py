import sys

import source_code.db_multi_choice_questions as multi_choice_Q
import source_code.db_insertion as db_insert
from source_code.data_collection import data_collect
from source_code.main_program import (
    new_file_name,
    selecting_existing_file,
    show_directory_files,
)
from source_code.view_game import view_games


def main_menu():
    """Loads the main menu of the program. Choose 'New', 'Edit' or 'Exit'."""
    while True:
        answer = multi_choice_Q.main_menu()

        if answer == "New":
            database_file = new_file_name()
            if database_file is not False:
                collected_data = data_collect(database_file)
                db_insert.db_insert(database_file, collected_data)
        elif answer == "Open":
            file_list = show_directory_files()
            if bool(file_list):
                database_file = selecting_existing_file(file_list)
                print(f"Current file is {database_file}")
                selection = multi_choice_Q.menu_open_options()
                if selection == "Search":
                    # TODO: place holder for search
                    view_game = view_games(database_file)
                elif selection == "Add":
                    collected_data = data_collect(database_file)
                    db_insert.db_insert(database_file, collected_data)
                elif selection == "Edit":
                    print("Edit")
                elif selection == "Delete":
                    print("Delete")
                elif selection == "Exit":
                    print("Exit")
            if not bool(file_list):
                print(
                    "There are currently no game database files in the save \
                        directory that can be opened."
                )
        elif answer == "Exit":
            sys.exit()
        else:
            print("Error getting the menu options.")
