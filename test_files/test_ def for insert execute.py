def execute_once(table: str, fields: list, database_values: list, c) -> None:
    """Inserts once into the database. Fields and database_values need
    to be tuples and the same lengh.

    :param table: This is the table name.
    :type table: str
    :param fields: Which fields you are populating.
    :type fields: list
    :param database_values: Values that are in be inserted in to each field.
    :type database_values: list
    :param c: The connection variable to the database.
    :type c: ?
    """
    try:
        number_of_q_marks = len(fields) - 1
        q_marks = "?, " * number_of_q_marks + "?"

        field_value = "".join(fields, ", ")
        database_value = "".join(database_values)

        # c.execute(
        print(f"INSERT INTO {table} ({field_value}) VALUES ({q_marks}), [{database_value}]"
        )

        return
    except Exception:
        print("Failed to Insert data into the databse.")

        return


table = "table_name"
fields = ["fig1", "fig2", "fig3"]
database_values = ["val1", "val2", "val3"]
c = "connection to the database, Not run here"

execute_once(table, fields, database_values, c)


# fields2 = ["fig1", "fig2", "fig3"]


# field_value2 = ""
# database_value = ""

# field_value2 = "".join(fields2)

# # for item in fields2:
# #     field_value2 += item + ", "
# print(fields2, "fig2")
# for item in database_values:
#     database_value += item
