import inquirer

d = {1:"a", 2:"b", 3:"c"}
l = ("a", "b", "c")

def print_dict():
    i = ""
    for k, v in d.items():
        print(k, v)
        i = i + k + v
        print(i)

j = [i for i in d.values()]


questions = [
        inquirer.Checkbox(
            "files",
            message="Please select the file",
            choices= lambda x : d.values(),
            default=None,
        )
    ]

answer = inquirer.prompt(questions)