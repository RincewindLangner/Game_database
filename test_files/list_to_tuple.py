from rich import print as rprint

# def list_to_tuple_list(item_list: list) -> list:
#     new_list = []
#     for item in item_list:
#         tuple = "(" + item + ")" + ","
#         new_list.append(tuple)
#     return new_list


# def list_to_tuple_list(item_list: list) -> list:
#     return [f"(\'{item}\')," for item in item_list]


# def list_to_tuple_list(item_list: list) -> list:
#     list_str = []
#     for item in item_list:
#         list_add = [f"({item}"]
#         rprint(list_add)
#         # list_str.append("(" + "'" + item + "'" + ")" + ",")
#     return [list_str]


def list_to_tuple_list(item_list: list) -> list:
    return [(item, ) for item in item_list]


var = [
    ("Steam",),
    ("GOG",),
    ("Epic",),
    ("Twitch / Amazon games",),
    ("Windows store",),
    ("Humble Bundle",),
    ("Other",),
]

old_list = [
    "N/A",
    "1",
    "2",
    "two",
    "gggg",
]

lt = list_to_tuple_list(old_list)

rprint(lt)
# rprint(type(lt[0]))
# rprint(len(lt))
