dict_1 = {"1": "a", "2": "b", "4": "d", "5": "x"}
dict_2 = {"1": "z", "2": "", "3": "c"}


# dict_2 |= dict_1
# dict_1 |= dict_2

# dict_2.update(dict_1)
dict_1.update(dict_2)


print(dict_1)
print(dict_2)
