from rich import print as rprint

# rprint("flg1")
# rprint(game_data["game_id"], game_data["genres"])
# rprint("flg2")
# rprint(game_data)
# rprint(type(game_data["genres"]))

# game__ids_genre_ids = [(game_data["game_id"], genre) for genre in game_data["genres"]]
# c.executemany(
#     "INSERT INTO genre_link (game_id, genre) VALUES \
#         (?, ?)",
#     game__ids_genre_ids,
# )


def list_two_values_tuple(item_id: dict, item_value: dict) -> list:
    """Takes two dict values and combines them into a list
    with containing a sets of tuples, each with two values.

    :param item_id: 1st dict values
    :type item_id: dict
    :param item_value: 2nd dict value
    :type item_value: dict
    :return: returns list with sets of tuples each with two values
    :rtype: list
    """
    return [(item_id, items) for items in item_value]


item_dict = {"game_id": 2, "nth_list": ["one", "two", "three", "four"]}


new_list = list_two_values_tuple(item_dict["game_id"], item_dict["nth_list"])

rprint(new_list)


# alt version with 'zip'

a = ("John", "Charles", "Mike")
b = ("Jenny", "Christy", "Monica", "Vicky")

x = zip(a, b)

# use the tuple() function to display a readable version of the result:

print(list(x))
