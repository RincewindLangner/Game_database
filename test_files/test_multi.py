import inquirer


class N_multiplayer:
    """Multiplayer for a single platform."""

    # def __init__(self, platform) -> None:
    #     self.platform = platform

    def __init__(self) -> None:
        pass

    def coop_local(self):
        """Select how many players for local co-op play."""
        question = [
            inquirer.List(
                "coop_local",
                message="How many for local co-op?",
                choices=["0", "2", "3", "4", "5", "6", "6+"],
            )
        ]

        answer = inquirer.prompt(question)

        return answer

    def coop_online(self):
        """Select how many players for online co-op play."""
        question = [
            inquirer.List(
                "coop_online",
                message="How many for online co-op?",
                choices=["0", "2", "3", "4", "5", "6", "6+"],
            )
        ]

        answer = inquirer.prompt(question)

        return answer

    def multi_local(self):
        """Select how many players for LAN / local multiplayer play."""
        question = [
            inquirer.List(
                "mulit_local",
                message="How many for LAN / local multiplayer?",
                choices=["0", "2", "3", "4", "5", "6", "6+"],
            )
        ]

        answer = inquirer.prompt(question)

        return answer

    def multi_online(self):
        """Select how many players for online multiplayer."""
        question = [
            inquirer.List(
                "coop_online",
                message="How many for online multiplayer?",
                choices=["0", "2", "3", "4", "5", "6", "6+"],
            )
        ]

        answer = inquirer.prompt(question)

        return answer


dict_multi = {
    "test1_key": "test1_value",
    "test2_key": "test2_value",
    "n_player": ["psx", "ps2", "ns"],
}

game_data = {
    "test1_key0": "test1_value0",
    "test2_key1": "test2_value1",
    "player": ["one", "two", "three"],
}


def multiplayer_coop(N_multiplayer, dict_multi, game_data):
    for item in dict_multi["n_player"]:
        platforms = f"{item}"

        i = N_multiplayer()

        cl = i.coop_local()
        clk = list(cl.keys())
        clk = platforms + "_" + str(clk[0])
        clv = list(cl.values())
        clv = str(clv[0])
        cld = {clk: clv}

        game_data = game_data | cld

        co = i.coop_online()
        cok = list(co.keys())
        cok = platforms + "_" + str(cok[0])
        cov = list(co.values())
        cov = str(cov[0])
        cod = {cok: cov}

        game_data = game_data | cod

    return game_data


def multiplayer(N_multiplayer, dict_multi, game_data):
    for item in dict_multi["n_player"]:
        platforms = f"{item}"

        i = N_multiplayer()

        cl = i.multi_local()
        clk = list(cl.keys())
        clk = platforms + "_" + str(clk[0])
        clv = list(cl.values())
        clv = str(clv[0])
        cld = {clk: clv}

        game_data = game_data | cld

        co = i.multi_online()
        cok = list(co.keys())
        cok = platforms + "_" + str(cok[0])
        cov = list(co.values())
        cov = str(cov[0])
        cod = {cok: cov}

        game_data = game_data | cod

    return game_data


game_data = multiplayer_coop(N_multiplayer, dict_multi, game_data)
game_data = multiplayer(N_multiplayer, dict_multi, game_data)

print(game_data)
