import sqlite3
from pathlib import Path
# from db_creation import create_db_file

game_data = {
    "game_id": "",
    "title": "iuyhgfpojhug",
    "description": "kjhgf",
    "publisher": "iuyt",
    "medium_of_game": ["Disc"],
}
# game_data = {'game_id': '', 'title': '2wefg', 'description': 'sdfghj', 'publisher': 'dfghjk', 'medium_of_game': ['Disc', 'Cartridge']}


print(game_data)
print(type(game_data))

db = "test.sqlite3"

create_db_file(db)

f_name = Path("test_files", db)

conn = sqlite3.connect(f_name)
conn.execute("PRAGMA foreign_keys = ON")
c = conn.cursor()


c.execute("""SELECT max(key_id) FROM games""")
max_game_id = c.fetchone()
if None in max_game_id:
    max_game_id = 1
else:
    max_game_id = max_game_id[0]
    max_game_id += 1
game_data["game_id"] = max_game_id

c.execute(
    """INSERT INTO games(title, description, publisher) VALUES (?,?,?)""",
    [game_data["title"], game_data["description"], game_data["publisher"]],
)

# conn.commit()

for item in game_data["medium_of_game"]:
    c.execute("""SELECT key_id FROM medium WHERE medium = ? """, (item,))
    medium_id = c.fetchone()
    c.execute(
        """INSERT INTO medium_link(medium_id, games_id) VALUES (?,?)""",
        [medium_id[0], game_data["game_id"]],
    )

# c.execute


conn.commit()
conn.close()

print("Database data inseted.")
