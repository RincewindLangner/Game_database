from pathlib import Path
import sqlite3


def create_db_file(db_file_name: str) -> None:
    """Pass a new file name complete with .sqlite3 extention.
    A new file and database tables will be
    created in the db_save_files folder."""
    # turn on foreign key support everytime you connect.
    conn, c = db_connect(db_file_name)
    try:

        # table creation
        c.executescript(
            """
        CREATE TABLE IF NOT EXISTS "age_rating" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "board"     TEXT,
            "rating"    TEXT,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "age_rating_other" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "board"     TEXT,
            "rating"    TEXT,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "cartridge_local_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "cartridge_online_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "cartridge_local_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "cartridge_online_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "digital_linux_local_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "digital_linux_local_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "digital_mac_local_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "digital_mac_local_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "disc_local_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "disc_local_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "disc_online_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "disc_online_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "digital_windows_local_coop" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "digital_windows_local_multiplayer" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "game_id"   INTEGER,
            "platform"  INTEGER,
            "number"    INTEGER,
            FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        );
        CREATE TABLE IF NOT EXISTS "games" (
            "game_id"   INTEGER NOT NULL,
            "title"     TEXT,
            "description"    TEXT,
            "publisher" TEXT,
            "developer" TEXT,
            "platform_other"    TEXT,
            PRIMARY KEY("game_id" AUTOINCREMENT)
        );
        CREATE TABLE IF NOT EXISTS "game_series" (
            "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
            "series"    TEXT, # No comma at the end or error
        );
        """
        # CREATE TABLE IF NOT EXISTS "game_series_link" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "series"    INTEGER,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id"),
        #     FOREIGN KEY("series") REFERENCES "game_series"("key_id")
        # );
        # CREATE TABLE IF NOT EXISTS "genre" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "genre"     TEXT,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # CREATE TABLE IF NOT EXISTS "medium_type" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "medium"    TEXT,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # CREATE TABLE IF NOT EXISTS "platform_digital_mac" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "platform"  TEXT,
        #     "year"      INTEGER,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # """
        # CREATE TABLE IF NOT EXISTS "platform_digital_linux" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "platform"  TEXT,
        #     "year"      INTEGER,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # CREATE TABLE IF NOT EXISTS "platform_digital_windows" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "platform"  TEXT,
        #     "year"      INTEGER,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # CREATE TABLE IF NOT EXISTS "platform_physical_cartridge" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "platform"  TEXT,
        #     "barcode"   TEXT,
        #     "year"      INTEGER,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # CREATE TABLE IF NOT EXISTS "platform_physical_disc" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "platform"  TEXT,
        #     "barcode"   INTEGER,
        #     "year"      INTEGER,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        # CREATE TABLE IF NOT EXISTS "theme" (
        #     "key_id"    INTEGER PRIMARY KEY AUTOINCREMENT,
        #     "game_id"   INTEGER,
        #     "theme"     TEXT,
        #     FOREIGN KEY("game_id") REFERENCES "games"("game_id")
        # );
        #     """
        )

        print(f"Database has been created called {db_file_name}")

    except Exception:
        # Roll back if fails
        db_rollback(conn)
    # TODO: add a retry option in the final version

    finally:
        db_commit_close(conn)
        return


def db_connect(db_file_name: str) -> str:
    """Connect to a database. Returns conn, c"""
    f_name = Path(db_file_name)
    f_name = f_name.resolve()
    conn = sqlite3.connect(f_name)
    conn.execute("PRAGMA foreign_keys = ON")
    c = conn.cursor()
    return conn, c


def db_commit_close(conn):
    """Commits and closes the database."""
    try:
        conn.commit()
        conn.close()
        return
    except Exception:
        db_rollback(conn)
        print("Failed to create database.")
        return


def db_rollback(conn):
    """Rollback and closes the database."""
    conn.rollback()
    print("Database failed to complete the task.")
    return


create_db_file("1.sqlite3")