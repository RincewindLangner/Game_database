def values_medium(state) -> str:
    """'qa' or 'db'. Returns [string], in square brackects, of options for use in the inquirer choises menu."""

    d = {1: "Discs", 2: "Cartridge", 3: "Digital", 4: "Other"}

    if state == "db":
        i = make_list_from_dict(d)

        j = []
        for x in i:
            a = "(" + '"' + x + '"' + "," + ")"
            j.append(a)

        return j

    if state == "qa":
        i = make_list_from_dict(d)

        return i


def make_list_from_dict(dict: dict) -> list:
    # sourcery skip: identity-comprehension, inline-immediately-returned-variable, list-comprehension, replace-dict-items-with-values, use-dict-items
    """To convert the dictionary to a list"""
    x = []
    x.extend(dict[value] for value in dict)
    print(x, "printing X")
    return x


print(values_medium("qa"))
print(values_medium("db"))
